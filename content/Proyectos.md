---
title: Proyectos
name: Oscar Cutipa Luque
date: 2017-02-20T15:26:23-06:00

---

Esta es mi lista de *proyectos*:

- Proyecto 1: Spatial point process for the detection of risk zones in traffic accidents in arequipa
- Proyecto 2: State-Space Modeling for Dengue in Peru by using Seasonal Variables

